Welcome to the MoneyTransfer 
==================================================

This sample code get you started with a simple Java application.

What's Here
-----------

This sample includes:

* README.md - this file
* pom.xml - this file is the Maven Project Object Model for the web application
* src/main - this directory contains your Java service source files
* src/test - this directory contains your Java service unit test files


Getting Started
---------------

To work on the sample code, you'll need to clone your project's repository to your
local computer. 

1. Build the application.

        $ mvn clean install

2. Goto the targert directory

        $ cd target

1. Run the executable jar

        $ java -jar moneytransfer-1.0.0-fat.jar 

5. Open http://127.0.0.1:8008/ in a Rest Client.

What Do I Do Next?
------------------

On running the application it incubates with a bank object and two accounts inside the bank.

```java
Name: 		Atul Girishkumar
AccountNo:  12500001 
Address: 	India 
Balance: 	300.0
```

```java
Name: 		Algirdas Zalatoris
AccountNo: 	12500002 
Address: 	London 
Balance: 	300.0
```

There are 4 RESTful APIS that can be consumed via this application


#### /transfer
```js 
http://127.0.0.1:8008/transer
This is a post request which transfers amount from one account to another. 
Post body ex: 
{
  "payer_account": 12500001,    // Payer's account id
  "payee_account": 12500002,    // Payee's account id
  "amount": 39                  // amount to be transfered
}
```
#### /createaccount
```js 
http://127.0.0.1:8008/createaccount
This is a post request which transfers amount from one account to another. 
Post body ex: 
{
  "account_name": "Kobe Bryant",     // Name of the account holder
  "account_address": "Los Angeles"   // Address of the account holder
}
```

#### /:accountId
```js 
http://127.0.0.1:8008/12500001
This is a get request which fetches the details of an account.
```
#### /:accountId/currentbalance
```js 
http://127.0.0.1:8008/12500001/currentbalance
This is a get request which fetches the current balance of an account.
```
#### /:accountId/transactions
```js 
http://127.0.0.1:8008/12500001/transactions
This is a get request which fetches all the transactions within an account.
```

