/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.revolut.interview.moneytransfer.validation;

import com.revolut.interview.moneytransfer.Constants;
import com.revolut.interview.moneytransfer.Main;
import com.revolut.interview.moneytransfer.exceptions.ValidationException;
import io.vertx.core.json.JsonObject;

/**
 *
 * @author Atul Girishkumar
 */
public class ValidationsImpl implements Validations {

    /**
     * Validates the input received for fund transfer between two accounts
     *
     * @param requestJson request body.
     * @throws ValidationException when a validation error occurs
     */
    @Override
    public void validateTransferInput(JsonObject requestJson) throws ValidationException {
        final long payerAccountId, payeeAccountId;
        // Validates payer's account id info
        if (requestJson.containsKey(Constants.PAYER_ACCOUNT)) {
            payerAccountId = requestJson.getLong(Constants.PAYER_ACCOUNT);
            if (!Main.getBank().hasAccount(payerAccountId)) {
                throw new ValidationException("Unable to find account with Id: " + payerAccountId);
            }
        } else {
            throw new ValidationException("Payer account id not found");
        }

        // Validates payee's account id info
        if (requestJson.containsKey(Constants.PAYEE_ACCOUNT)) {
            payeeAccountId = requestJson.getLong(Constants.PAYEE_ACCOUNT);
            if (!Main.getBank().hasAccount(payerAccountId)) {
                throw new ValidationException("Unable to find account with Id: " + payeeAccountId);
            }
        } else {
            throw new ValidationException("Payee account id not found");
        }

        // Validation for same payee and payer account id
        if (requestJson.getLong(Constants.PAYER_ACCOUNT).equals(requestJson.getLong(Constants.PAYEE_ACCOUNT))) {
            throw new ValidationException("Payee and Payer accountId is same");
        }

        // Validates the transfer amount
        if (requestJson.containsKey(Constants.AMOUNT)) {
            if (requestJson.getDouble(Constants.AMOUNT) < 0) {
                throw new ValidationException("Transefer Amount cannot be less that zero");
            }
        } else {

        }
    }

    /**
     * Validates the input received for creation of a user account.
     *
     * @param requestJson request body.
     * @throws ValidationException when a validation error occurs
     */
    @Override
    public void validateCreateAccountInput(JsonObject requestJson) throws ValidationException {
        if (!requestJson.containsKey(Constants.ACCOUNT_NAME)) {
            throw new ValidationException("Please provide account holder name");
        } else {
            String accountName = requestJson.getString(Constants.ACCOUNT_NAME);
            String accountAddress = requestJson.getString(Constants.ACCOUNT_ADDRESS);
            boolean result = Main.getBank().getAccounts().stream().anyMatch((t) -> t.getAccountHolderName().equalsIgnoreCase(accountName) && t.getAddress().equalsIgnoreCase(accountAddress));
            if (result) {
                throw new ValidationException("Please provide different account holder name/address");
            }
        }
        if (!requestJson.containsKey(Constants.ACCOUNT_ADDRESS)) {
            throw new ValidationException("Please provide account holder address");
        }
    }

}
