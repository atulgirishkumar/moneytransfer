/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.revolut.interview.moneytransfer.validation;

import com.revolut.interview.moneytransfer.exceptions.ValidationException;
import io.vertx.core.json.JsonObject;

/**
 * Defines the validation for inputs to the system
 * @author Atul Girishkumar
 */
public interface Validations {
    
    /**
     * Validates the input received for fund transfer between two accounts
     * @param requestJson request body.
     * @throws ValidationException when a validation error occurs
     */
    public void validateTransferInput(JsonObject requestJson) throws ValidationException;

    /**
     * Validates the input received for creation of a user account.
     * @param requestJson request body.
     * @throws ValidationException when a validation error occurs
     */
    public void validateCreateAccountInput(JsonObject requestJson) throws ValidationException;
    
}
