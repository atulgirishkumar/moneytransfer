/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.revolut.interview.moneytransfer.exceptions;

/**
 * Used to throw in case of a validation anomaly
 * @author Atul Girishkumar
 */
public class ValidationException extends Exception{
    private String message;
    private Exception exception;
            
    /**
     *
     * @param message message for exception
     */
    public ValidationException(String message) {
        this.message = message;
    }
    
    /**
     *
     * @param exception object of the exception
     */
    public ValidationException(Exception exception){
        this.exception = exception;
    }
    
    /**
     * 
     * @return exception message
     */
    public String getMessage() {
        return message;
    }

    /**
     *
     * @return exception object
     */
    public Exception getException() {
        return exception;
    }
}
