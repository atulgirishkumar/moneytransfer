/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.revolut.interview.moneytransfer.operations;

import io.vertx.ext.web.RoutingContext;

/**
 * This class consists of the list of banking operations
 * 
 * @author Atul Girishkumar
 */
public interface Operations {
    /**
     * Transfers funds between two accounts
     * @param routingContext represents the context for the handling of a web request.
     */
    public void transferFunds(RoutingContext routingContext);
    
    /**
     * Fetches all the transactions of an account
     * @param routingContext represents the context for the handling of a web request.
     */
    public void viewTransactions(RoutingContext routingContext);
    
    /**
     * Fetches the balance of an account
     * @param routingContext represents the context for the handling of a web request.
     */
    public void getCurrentBalance(RoutingContext routingContext);

    /**
     * Fetches the getAccountDetails of an account
     *
     * @param routingContext represents the context for the handling of a web
     * request.
     */
    public void getAccountDetails(RoutingContext routingContext);
    
    /**
     * Creates a user account
     * @param routingContext represents the context for the handling of a web request.
     */
    public void createUserAccount(RoutingContext routingContext);
   
}
