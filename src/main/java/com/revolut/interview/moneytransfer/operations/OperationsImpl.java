/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.revolut.interview.moneytransfer.operations;

import com.revolut.interview.moneytransfer.Constants;
import com.revolut.interview.moneytransfer.Main;
import com.revolut.interview.moneytransfer.exceptions.ValidationException;
import com.revolut.interview.moneytransfer.model.Account;
import com.revolut.interview.moneytransfer.validation.Validations;
import com.revolut.interview.moneytransfer.validation.ValidationsImpl;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.RoutingContext;
import java.time.Instant;

/**
 * This class is an implementation of the list of banking operation defined by
 * the <code>Operations</code> Interface
 *
 * @author Atul Girishkumar
 */
public class OperationsImpl implements Operations {

    private static final Logger LOG = LoggerFactory.getLogger(Constants.LOGGER);

    /**
     * Transfers funds between two accounts
     *
     * @param routingContext represents the context for the handling of a web
     * request.
     */
    @Override
    public void transferFunds(RoutingContext routingContext) {
        LOG.info("Inside transfer method");
        routingContext.request().bodyHandler((handler) -> {
            synchronized (this) {
                String responseMessage = "";
                String handlerString = handler.toString();
                if (!handlerString.isEmpty()) {
                    try {
                        JsonObject requestJson = new JsonObject(handlerString);

                        Validations validations = new ValidationsImpl();
                        validations.validateTransferInput(requestJson);

                        Account payerAccount = Main.getBank().getAccount(requestJson.getLong(Constants.PAYER_ACCOUNT));
                        Account payeeAccount = Main.getBank().getAccount(requestJson.getLong(Constants.PAYEE_ACCOUNT));

                        if (payerAccount.getBalance() >= requestJson.getDouble(Constants.AMOUNT)) {
                            Double payerTotalAmount = payerAccount.getBalance() - requestJson.getDouble(Constants.AMOUNT);
                            Double payeeTotalAmount = payeeAccount.getBalance() + requestJson.getDouble(Constants.AMOUNT);

                            payerAccount.setBalance(payerTotalAmount);
                            LOG.trace("Updating balance for account number: " + payerAccount.getAccountNumber());

                            payeeAccount.setBalance(payeeTotalAmount);
                            LOG.trace("Updating balance for account number: " + payeeAccount.getAccountNumber());

                            payerAccount.addTransactions("Timestamp: " + Instant.now() + " " + requestJson.getDouble(Constants.AMOUNT) + "USD debited to " + payeeAccount.getAccountNumber());
                            payeeAccount.addTransactions("Timestamp: " + Instant.now() + " " + requestJson.getDouble(Constants.AMOUNT) + "USD credited from " + payerAccount.getAccountNumber());

                            // Updating the storage
                            Main.getBank().updateAccount(payerAccount);
                            Main.getBank().updateAccount(payeeAccount);
                            responseMessage = "Timestamp: " + Instant.now() + " Transfered " + requestJson.getDouble(Constants.AMOUNT) + " USD Successfully to " + payerAccount.getAccountHolderName();
                            LOG.debug(responseMessage);
                        } else {
                            LOG.trace("Unable to complete the transfer due to insuficient funds");
                            responseMessage = "Insuficient funds";
                        }
                    } catch (ValidationException validationException) {
                        LOG.error("ValidationException occured during transfer ", validationException);
                        responseMessage = validationException.getMessage();
                    } catch (Exception exception) {
                        LOG.error("Exception occured during transfer ", exception);
                        responseMessage = exception.getMessage();
                    }
                } else {
                    LOG.trace("Empty input received");
                    responseMessage = "Empty input";
                }
                routingContext.response().end(responseMessage);
            }
        });

    }

    /**
     * Fetches all the transactions of an account
     *
     * @param routingContext represents the context for the handling of a web
     * request.
     */
    @Override
    public void viewTransactions(RoutingContext routingContext) {
        LOG.info("Inside view transactions method");
        String responseMessage = "";
        try {
            long accountId = Long.parseLong(routingContext.request().getParam("accountId"));
            if (Main.getBank().hasAccount(accountId)) {
                int counter = 1;
                for (String transaction : Main.getBank().getAccount(accountId).getTransactions()) {
                    responseMessage += counter + ". " + transaction + "\n";
                    counter++;
                }
                LOG.debug(responseMessage);
            } else {
                LOG.trace("Unable to find user account");
                responseMessage = "Not a valid user account";
            }
        } catch (Exception exception) {
            LOG.error("Exception occured during transfer ", exception);
            responseMessage = exception.getLocalizedMessage();
        }
        routingContext.response().end(responseMessage);
    }

    /**
     * Fetches the balance of an account
     *
     * @param routingContext represents the context for the handling of a web
     * request.
     */
    @Override
    public void getCurrentBalance(RoutingContext routingContext) {
        LOG.info("Inside check balance method");
        String responseMessage = "";
        try {
            long accountId = Long.parseLong(routingContext.request().getParam("accountId"));
            if (Main.getBank().hasAccount(accountId)) {
                Double balance = Main.getBank().getAccount(accountId).getBalance();
                responseMessage = "The current balance is " + balance;
                LOG.debug(responseMessage);
            } else {
                LOG.trace("Unable to find user account");
                responseMessage = "Not a valid user account";
            }
        } catch (Exception exception) {
            LOG.error("Exception occured during transfer ", exception);
            responseMessage = exception.getLocalizedMessage();
        }
        routingContext.response().end(responseMessage);
    }

    /**
     * Fetches the getAccountDetails of an account
     *
     * @param routingContext represents the context for the handling of a web
     * request.
     */
    @Override
    public void getAccountDetails(RoutingContext routingContext) {
        LOG.info("Inside get account details method");
        String responseMessage = "";
        try {
            long accountId = Long.parseLong(routingContext.request().getParam("accountId"));
            if (Main.getBank().hasAccount(accountId)) {
                Account account = Main.getBank().getAccount(accountId);
                responseMessage = account.toString();
                LOG.debug(responseMessage);
            } else {
                LOG.trace("Unable to find user account");
                responseMessage = "Not a valid user account";
            }
        } catch (Exception exception) {
            LOG.error("Exception occured during transfer ", exception);
            responseMessage = exception.getLocalizedMessage();
        }
        routingContext.response().end(responseMessage);
    }

    /**
     * Creates a user account
     *
     * @param routingContext represents the context for the handling of a web
     * request.
     */
    @Override
    public void createUserAccount(RoutingContext routingContext) {
        LOG.info("Inside create user account method");
        routingContext.request().bodyHandler((handler) -> {
            synchronized (this) {
                String responseMessage = "";
                String handlerString = handler.toString();
                if (!handlerString.isEmpty()) {
                    try {
                        JsonObject requestJson = new JsonObject(handlerString);

                        Validations validations = new ValidationsImpl();
                        validations.validateCreateAccountInput(requestJson);

                        Account userAccount = new Account(requestJson.getString(Constants.ACCOUNT_NAME), requestJson.getString(Constants.ACCOUNT_ADDRESS));
                        Main.getBank().addAccount(userAccount);

                        responseMessage = userAccount.toString();
                        LOG.debug(responseMessage);
                    } catch (ValidationException validationException) {
                        LOG.error("ValidationException occured during transfer ", validationException);
                        responseMessage = validationException.getMessage();
                    } catch (Exception exception) {
                        LOG.error("Exception occured during transfer ", exception);
                        responseMessage = exception.getMessage();
                    }
                } else {
                    LOG.trace("Empty input received");
                    responseMessage = "Empty input";
                }
                routingContext.response().end(responseMessage);
            }
        });
    }

}
