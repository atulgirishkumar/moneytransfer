/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.revolut.interview.moneytransfer;

/**
 * Defines all the constant
 * 
 * @author Atul Girishkumar
 */
public final class Constants {

    public static final String AMOUNT = "amount";
    public static final String PAYER_ACCOUNT = "payer_account";
    public static final String PAYEE_ACCOUNT = "payee_account";
    public static final String ACCOUNT_NAME = "account_name";
    public static final String ACCOUNT_ADDRESS = "account_address";
    public static final String LOGGER = "AppLogger";
}
