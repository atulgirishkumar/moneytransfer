/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.revolut.interview.moneytransfer;

import com.revolut.interview.moneytransfer.operations.Operations;
import com.revolut.interview.moneytransfer.operations.OperationsImpl;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServer;
import io.vertx.ext.web.Router;

/**
 * This class deploys a HTTP server which can be used to consume RESTful apis.
 * @author Atul Girishkumar
 */
public class HTTPService extends AbstractVerticle {

    /**
     * This method is triggered when we deploy this class. 
     * It creates an HTTP server and defines the API routes that can be consumed for banking operations.
     * @param startFuture object of the future as the call is asynchronous
     * @throws Exception in case of unexpected event execution.
     */    
    @Override
    public void start(Future<Void> startFuture) throws Exception {
        System.out.println("Deploying HTTPService");

        final Router router = Router.router(vertx);
        final HttpServer server = vertx.createHttpServer().requestHandler(router).listen(8008, "localhost", httpServer -> {
            if (httpServer.succeeded()) {
                System.out.println("HTTP server started on port 8008");
                startFuture.complete();
            }else{
                startFuture.fail(httpServer.cause());
            }
        });
        
        // Operations object
        final Operations operations = new OperationsImpl();
        
        router.route(HttpMethod.POST, "/transfer").handler(operations::transferFunds);
        router.route(HttpMethod.POST, "/createaccount").handler(operations::createUserAccount);
        router.route(HttpMethod.GET, "/:accountId").handler(operations::getAccountDetails);
        router.route(HttpMethod.GET, "/:accountId/transactions").handler(operations::viewTransactions);
        router.route(HttpMethod.GET, "/:accountId/currentbalance").handler(operations::getCurrentBalance);
        router.route(HttpMethod.GET, "/").handler((rountingContext) -> {
            rountingContext.response().end("Welcome to moneytransfer app");
        });
    }

}
