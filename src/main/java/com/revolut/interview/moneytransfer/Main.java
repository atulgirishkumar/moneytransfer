/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.revolut.interview.moneytransfer;

import com.revolut.interview.moneytransfer.model.Account;
import com.revolut.interview.moneytransfer.model.Bank;
import io.vertx.core.Vertx;

/**
 * This is the main class
 *
 * @author Atul Girishkumar
 */
public class Main {

    private static Vertx vertx;
    private static Bank bank;

    /**
     * Starting point of the application
     *
     * @param args
     */
    public static void main(String[] args) {
        vertx = Vertx.vertx();
        init();
        vertx.deployVerticle("com.revolut.interview.moneytransfer.HTTPService");
    }

    /**
     * Initializes the required data.
     * Creates two account and adds those account to a bank.
     */
    public static void init() {
        bank = new Bank("MoneyTransfer", "london", "LON000024");

        Account userAccount1 = new Account("Atul Girishkumar", "India");
        bank.addAccount(userAccount1);
        System.out.println("User Account created for " + userAccount1.getAccountHolderName() + " with accound Id " + userAccount1.getAccountNumber());

        Account userAccount2 = new Account("Algirdas Zalatoris", "London");
        bank.addAccount(userAccount2);
        System.out.println("User Account created for " + userAccount2.getAccountHolderName() + " with accound Id " + userAccount2.getAccountNumber());
    }

    /**
     *
     * @return instance of bank
     */
    public static Vertx getVertx() {
        return vertx;
    }

    /**
     *
     * @return instance of bank
     */
    public static Bank getBank() {
        return bank;
    }
}
