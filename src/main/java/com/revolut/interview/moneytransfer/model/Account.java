/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.revolut.interview.moneytransfer.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Bean class for representation of Account
 *
 * @author Atul Girishkumar
 */
public class Account {

    private static long accountNumerValue = 12500000;
    private String accountHolderName;
    private String address;
    private final long accountNumber;
    private Double balance;
    private final List<String> transactions = new ArrayList<>();

    /**
     * Creates an account
     *
     * @param accountHolderName name of the account holder
     * @param address address of the account holder
     */
    public Account(String accountHolderName, String address) {
        this.accountHolderName = accountHolderName;
        this.address = address;
        this.accountNumber = ++accountNumerValue;
        this.balance = 300d;
        transactions.add("Account created with balance USD " + balance);
    }

    /**
     *
     * @return current balance
     */
    public Double getBalance() {
        return balance;
    }

    /**
     * Sets the current account balance.
     *
     * @param amount amount to set as the balance
     */
    public void setBalance(Double amount) {
        this.balance = amount;
    }

    /**
     *
     * @return the account holder name
     */
    public String getAccountHolderName() {
        return accountHolderName;
    }

    /**
     * Sets the name of the account holder
     *
     * @param accountHolderName name to be set
     */
    public void setAccountHolderName(String accountHolderName) {
        this.accountHolderName = accountHolderName;
    }

    /**
     *
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * Sets the address of the account holder
     *
     * @param address address to be set
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     *
     * @return the account number
     */
    public long getAccountNumber() {
        return accountNumber;
    }

    /**
     * Adds the given transaction message to the list of transactions
     *
     * @param transactionMessage transaction message
     */
    public void addTransactions(String transactionMessage) {
        this.transactions.add(transactionMessage);
    }

    /**
     *
     * @return all the transactions
     */
    public List<String> getTransactions() {
        return transactions;
    }

    @Override
    public String toString() {
        return "Name: \t\t" + getAccountHolderName() + "\nAccount No: \t" + getAccountNumber() + " \nAddress: \t" + getAddress() + " \nBalance: \t" + getBalance();
    }

}
