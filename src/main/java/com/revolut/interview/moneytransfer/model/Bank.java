/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.revolut.interview.moneytransfer.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Bean class for representation of Bank
 *
 * @author Atul Girishkumar
 */
public class Bank {

    private String name;
    private String address;
    private final String ifscCode;
    private final Map<Long, Account> listofAccounts = new HashMap<>();

    /**
     * Creates a bank
     *
     * @param name name of the bank
     * @param address address of the bank
     * @param ifscCode ifsc code of the bank.
     */
    public Bank(String name, String address, String ifscCode) {
        this.name = name;
        this.address = address;
        this.ifscCode = ifscCode;
    }

    /**
     *
     * @return the ifsc code of the bank
     */
    public String getIfscCode() {
        return ifscCode;
    }

    /**
     *
     * @return name of the bank
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name of the bank
     *
     * @param name name of the bank
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return address of the bank
     */
    public String getAddress() {
        return address;
    }

    /**
     * Sets the address of the bank
     *
     * @param address address of the bank
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * Adds an account to the bank
     *
     * @param account account to be added to the bank
     */
    public void addAccount(Account account) {
        this.listofAccounts.put(account.getAccountNumber(), account);
    }

    /**
     * Updates and account to the bank
     *
     * @param account account to be updated to the bank
     */
    public void updateAccount(Account account) {
        this.addAccount(account);
    }

    /**
     * Check whether an account is present in the bank
     *
     * @param accountNumber account number to be check
     * @return true or false
     */
    public boolean hasAccount(long accountNumber) {
        if (this.listofAccounts.containsKey(accountNumber)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Fetches an account from the list of accounts
     *
     * @param accountNumber account number to be fetched
     * @return
     */
    public Account getAccount(long accountNumber) {
        return listofAccounts.get(accountNumber);
    }

    /**
     *
     * @return all the accounts Ids
     */
    public Set<Long> getAccountsIds() {
        return listofAccounts.keySet();
    }
    
    /**
     *
     * @return all the accounts
     */
    public List<Account> getAccounts() {
        return listofAccounts.entrySet().stream().map((t) -> t.getValue()).collect(Collectors.toList());
    }

}
