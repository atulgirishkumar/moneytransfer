package io.moneytransfer.core;

import com.revolut.interview.moneytransfer.HTTPService;
import com.revolut.interview.moneytransfer.Main;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.codec.BodyCodec;
import io.vertx.junit5.Checkpoint;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;
import static org.assertj.core.api.Java6Assertions.assertThat;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

/**
 * Defines test cases for the following actions 1. Transfer of amount between
 * accounts. 2. Fetching account details 3. Fetching all the transactions of an
 * account
 *
 * @author Atul Girishkumar
 */
@ExtendWith(VertxExtension.class)
public class TestMainVerticle {

    /**
     * Initializes the data for test suite
     *
     * @param vertx instance of vertx
     * @param testContext A test context to wait on the outcomes of asynchronous
     * operations.
     * @throws Throwable in case of any exception
     */
    @BeforeAll
    static void intialization(VertxTestContext testContext) throws Throwable {
        Main.init();
        testContext.completeNow();
    }

    /**
     * Deploys the http server
     *
     * @param vertx instance of vertx
     * @param testContextA test context to wait on the outcomes of asynchronous
     * operations.
     */
    @BeforeEach
    void deploy_verticle(Vertx vertx, VertxTestContext testContext) {
        vertx.deployVerticle(new HTTPService(), testContext.succeeding(id -> testContext.completeNow()));
    }

    /**
     * Test the action of listing of transaction for a single account
     *
     * @param vertx instance of vertx
     * @param testContext A test context to wait on the outcomes of asynchronous
     * operations.
     * @throws Throwable in case of any exception
     */
    @Test
    void list_all_account_transactions(Vertx vertx, VertxTestContext testContext) throws Throwable {
        System.out.println("\n\nExecuting fetch account transactions test case");
        WebClient webClient = WebClient.create(vertx);
        // When all created checkpoints have been flagged, then a test succeeds. By default 1.
        Checkpoint requestCheckpoint = testContext.checkpoint();

        webClient.get(8008, "localhost", "/12500001/transactions")
                .as(BodyCodec.string())
                .send(testContext.succeeding(resp -> {
                    testContext.verify(() -> {
                        System.out.println("Status code: " + resp.statusCode());
                        System.out.println("Response: \n" + resp.body());
                        assertThat(resp.statusCode()).isEqualTo(200);
                        requestCheckpoint.flag();
                        System.out.println("Finished executing fetch account transactions test case\n\n");
                    });
                }));
    }

    /**
     * Test the action of transfer of amount between two accounts
     *
     * @param vertx instance of vertx
     * @param testContext A test context to wait on the outcomes of asynchronous
     * operations.
     * @throws Throwable in case of any exception
     */
    @Test
    void account_transfer(Vertx vertx, VertxTestContext testContext) throws Throwable {
        System.out.println("\n\nExecuting transfer amount between account test case");
        WebClient webClient = WebClient.create(vertx);
        // When all created checkpoints have been flagged, then a test succeeds. By default 1.
        Checkpoint requestCheckpoint = testContext.checkpoint();

        JsonObject jsonObject = new JsonObject();
        jsonObject.put("payer_account", 12500001);
        jsonObject.put("payee_account", 12500002);
        jsonObject.put("amount", 39);

        webClient.post(8008, "localhost", "/transfer")
                .as(BodyCodec.string())
                .sendJson(jsonObject, testContext.succeeding(resp -> {
                    testContext.verify(() -> {
                        System.out.println("Status code: " + resp.statusCode());
                        assertThat(resp.statusCode()).isEqualTo(200);
                        requestCheckpoint.flag();
                        System.out.println("Finished executing transfer amount between account test case\n\n");
                    });
                }));
    }

    /**
     * Test the action of fetching account details
     *
     * @param vertx instance of vertx
     * @param testContext A test context to wait on the outcomes of asynchronous
     * operations.
     * @throws Throwable in case of any exception
     */
    @Test
    void account_details(Vertx vertx, VertxTestContext testContext) throws Throwable {
        System.out.println("\n\nExecuting fetch account details test case");
        WebClient webClient = WebClient.create(vertx);
        // When all created checkpoints have been flagged, then a test succeeds. By default 1.
        Checkpoint requestCheckpoint = testContext.checkpoint();

        webClient.get(8008, "localhost", "/12500001")
                .as(BodyCodec.string())
                .send(testContext.succeeding(resp -> {
                    testContext.verify(() -> {
                        System.out.println("Status code: " + resp.statusCode());
                        System.out.println("Response: \n" + resp.body());
                        assertThat(resp.statusCode()).isEqualTo(200);
                        requestCheckpoint.flag();
                        System.out.println("Finished executing fetch account details test case\n\n");
                    });
                }));
    }

    /**
     * Test the action of creating user account
     *
     * @param vertx instance of vertx
     * @param testContext A test context to wait on the outcomes of asynchronous
     * operations.
     * @throws Throwable in case of any exception
     */
    @Test
    void create_useraccount(Vertx vertx, VertxTestContext testContext) throws Throwable {
        System.out.println("\n\nExecution create user account test case");
        WebClient webClient = WebClient.create(vertx);
        // When all created checkpoints have been flagged, then a test succeeds. By default 1.
        Checkpoint requestCheckpoint = testContext.checkpoint();

        JsonObject jsonObject = new JsonObject();
        jsonObject.put("account_name", "Kobe Bryant");
        jsonObject.put("account_address", "Los Angeles");

        webClient.post(8008, "localhost", "/createaccount")
                .as(BodyCodec.string())
                .sendJson(jsonObject, testContext.succeeding(resp -> {
                    testContext.verify(() -> {
                        System.out.println("Status code: " + resp.statusCode());
                        System.out.println("Response: \n" + resp.body());
                        assertThat(resp.statusCode()).isEqualTo(200);
                        requestCheckpoint.flag();
                        System.out.println("Finished executing transfer amount between account test case\n\n");
                    });
                }));
    }

}
